
function init(){
    loadScores();
    loadGames();
    loadAdminPanel();
}

function loadGames(){
    $.ajax({
        type: "GET",
        url: "/api/quizzs",
        dataType: "json",
        success: function(json) {
            var data = Array();
            var max = 0;
            $.each(json["quizzs"], function(key, val){
                console.log(val);
                $("#game").append(jsonGameToHtml(val));
            })
        }
    });

}

function loadScores(){

}

function loadAdminPanel(){
    $.ajax({
        type: "GET",
        url: "/api/quizzs/me",
        dataType: "json",
        success: function(json) {
            $.each(json["quizzs"], function(key, val){
                quizzLine = jsonQuizzToHtml(val);
                $("#mesQuizzs").append(quizzLine);
            })
        }
    });

}

function Quizz(name){
    this.name = name;
}

function newQuizz(){
    var quizz = new Quizz($("#quizzName").val());
    console.log("Création du quizz "+JSON.stringify(quizz));
    $.ajax({
        url : "/api/quizzs",
        type: "POST",
        contentType: "application/json",
        data: JSON.stringify(quizz),
        datatype: "json",
        success: loadAdminPanel
    });
}


function playQuizz(idQuizz){
    $("#game").empty();
    $("#game").append("<div id=\"questions\"></div>");
    $.ajax({
        type: "GET",
        url: "/api/quizzs/"+idQuizz+"/questions",
        dataType: "json",
        success: function(json) {
            $.each(json["questions"], function(key, val){
                $("#questions").append(jsonQuestionToHtml(val));
            })
            $("#questions").append("<div class=\"btn btn-default\" onclick=\"correc();\">Correction</div>");
        }
    });
}
function jsonQuestionToHtml(json){
    var reponse
    if(json["firstChecked"]){
        reponse="r1";
    }else{
        reponse="r2";
    }
    res = ""
    +"<h4>"+json["title"]+"</h4>"
    +"<div class=\"funkyradio question\" data-res=\"q"+json["id"]+reponse+"\">"
    +"    <div class=\"funkyradio-primary\">"
    +"        <input type=\"radio\" name=\"radioq"+json["id"]+"\" id=\"q"+json["id"]+"r1\" />"
    +"        <label for=\"q"+json["id"]+"r1\">"+json["firstAlternative"]+"</label>"
    +"    </div>"
    +"    <div class=\"funkyradio-primary\">"
    +"        <input type=\"radio\" name=\"radioq"+json["id"]+"\" id=\"q"+json["id"]+"r2\" />"
    +"        <label for=\"q"+json["id"]+"r2\">"+json["secondAlternative"]+"</label>"
    +"    </div>"
    +"</div>";

    return res;
}

function correc(){
    $.each($(".question"), function(key, val){
		var selecteur = "#"+val.getAttribute("data-res")

		console.log($("#"+val.getAttribute("data-res")).attr("checked", "checked"));

		if($("#"+val.getAttribute("data-res")).checked )

			{alert("BONNE REPONSE")}
		else
			{alert("MAUVAISE REPONSE")}
	});
}

function modifQuizz(idQuizz){
    $("#mesQuizzs").empty();
    $.ajax({
        type: "GET",
        url: "/api/quizzs/"+idQuizz,
        dataType: "json",
        success: function(json) {
            $("#mesQuizzs").append("<h4>"+json["name"]+"</h4>");
            $("#mesQuizzs").append("<hr />");
            $("#mesQuizzs").append("<div id=\"debutQuestions\"></div>");
            $("#mesQuizzs").append("<hr />");
            $("#mesQuizzs").append(formNewQuestion(idQuizz));
            $.ajax({
                type: "GET",
                url: "/api/quizzs/"+idQuizz+"/questions",
                dataType: "json",
                success: function(json) {
                    $.each(json["questions"], function(key, val){
                        console.log(val);
                        $("#debutQuestions").append("<h5>"+val["title"]+"</h5>");
                    })
                }
            });
        }
    });
}

function formNewQuestion(idQuizz){

    res = ""
    +"<form>"
    +"    <h4> Nouvelle question </h4>"
    +"    <div class=\"form-group\" id=\"questionTitle\">"
    +"        <label for=\"Nom\">Intitulé de la question</label>"
    +"        <input type=\"text\" class=\"form-control\" id=\"questName\" placeholder=\"Intitulé\">"
    +"    </div>"
    +"    <div class=\"form-group\" id=\"questionRep1\">"
    +"        <label for=\"Nom\">Réponse 1</label>"
    +"        <input type=\"text\" class=\"form-control\" id=\"questR1\" placeholder=\"Réponse1\">"
    +"    </div>"
    +"    <div class=\"form-group\" id=\"questionRep2\">"
    +"        <label for=\"Nom\">Réponse 2</label>"
    +"        <input type=\"text\" class=\"form-control\" id=\"questR2\" placeholder=\"Réponse2\">"
    +"    </div>"
    +"    <div class=\"form-group\" id=\"questionCorrec\">"
    +"        <label for=\"Nom\">Quel est la bonne réponse?</label>"
    +"        <select class=\"form-control\" id=\"questionCorrec\">"
    +"            <option value=\"1\">1</option>"
    +"            <option value=\"2\">2</option>"
    +"        </select>"
    +"    </div>"
    +"    <div class=\"btn btn-default\" onclick=\"newQuest("+idQuizz+");\">Ajouter</div>"
    +"</form>";

    return res;
}

function Question(quizz_id,title,firstAlternative,secondAlternative,firstChecked){
    this.quizz_id = quizz_id ;
    this.title = title ;
    this.firstAlternative = firstAlternative ;
    this.secondAlternative = secondAlternative ;
    this.firstChecked = firstChecked ;
}

function newQuest(idQuizz){

    firstChecked = $("#questionCorrec option:selected").val() == "1";

    var question = new Question(
        idQuizz,
        $("#questName").val(),
        $("#questR1").val(),
        $("#questR2").val(),
        firstChecked
    );
    console.log("Création de la question "+JSON.stringify(question));

    $.ajax({
        url : "/api/questions",
        type: "POST",
        contentType: "application/json",
        data: JSON.stringify(question),
        datatype: "json"
    });

    modifQuizz(idQuizz);

}



function jsonQuizzToHtml(json){
    rep = ""
    +"<div class=\"adminMonQuizz btn btn-default\" onclick=\"modifQuizz("+json["id"]+")\">"
    +"<h5>"+json["name"]+"</h5>"
    +"</div>"
    return rep;
}
function jsonGameToHtml(json){
    rep = ""
    +"<div class=\"adminMonQuizz btn btn-default\" onclick=\"playQuizz("+json["id"]+")\">"
    +"<h5>"+json["name"]+"<br />Par "+json["creator"]+"</h5>"
    +"</div>"
    return rep;
}


function testFunc(){
    var html="";
    $.ajax({
        type: "GET",
        url: "/api/quizzs/1",
        dataType: "json",
        success: function(json) {
            html = jsonQuizzToHtml(json);
        }
    });
    return html;
}
//
// function saveNewSondage(){
// 	var sond = new Sondage($("#sondages #newSond #nameNewSond").val());
// 	console.log("Creation of new sondage object\n"+JSON.stringify(sond));

// 	$("#newSond").remove();
// 	refreshSondagesList();
// }

/*
function ChargerQuizz(){
	$.ajax({
		type: "GET",
		url: "/api/sondages",
		dataType: "json",
		success: function(json){

			$.each(json["sondages"], function(key, val){
				console.log(val);
				$("#jeu").append("<button type=\"button\" class=\"list-group-item\" onclick=\"jouer("+val["id"]+")\">"+val["name"]+"</button>");

		})
		}

	});
}

function jouer(idQuizz){
	console.log(idQuizz);
	$("#jeu").empty();
	$.ajax({
		type: "GET",
		url: "/api/sondages/"+idQuizz,
		dataType: "json",
		success: function(json){
	console.log(json["name"]);

				$("#jeu").append("<h1>"+json["name"]+"</h1>");
		}

	});

	$.ajax({
		type: "GET",
		url: "/api/sondages/"+idQuizz+"/questions",
		dataType: "json",
		success: function(json){
			console.log(json);
			$.each(json["questions"], function(key, val){
				console.log(val);
				$("#jeu").append("<button type=\"button\" class=\"list-group-item\" onclick=\"jouer("+val["id"]+")\">"+val["title"]+"</button>");

		})
		}

	});
}
*/
