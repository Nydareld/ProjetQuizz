from .app import app, db
from flask import render_template, request, jsonify, abort,url_for,redirect
from .models import User, Quizz, Question, SimpleQuestion
from flask.ext.wtf import Form,RecaptchaField
from wtforms import StringField, HiddenField,PasswordField,SelectField,TextAreaField,validators
from flask.ext.login import login_user,current_user,logout_user,login_required
from wtforms.validators import DataRequired
from sqlalchemy.exc import IntegrityError
from hashlib import sha256


class SigninForm(Form):
	username=StringField('Username',validators=[DataRequired()])
	password=PasswordField('Password',validators=[DataRequired()])
	next=HiddenField()

class LoginForm(Form):
	username=StringField('Username',validators=[DataRequired()])
	password=PasswordField('Password',validators=[DataRequired()])
	next=HiddenField()

	def get_authenticated_user(self):
		user=User.query.get(self.username.data)
		if user is None:
			return None
		m=sha256()
		m.update(self.password.data.encode())
		passwd=m.hexdigest()
		return user if passwd==user.password else None

@app.route("/")
def home():
	f=LoginForm()
	return render_template(
		"accueil.html",
		title="Quizz",form=f)


@app.route("/test")
def test():
	f=LoginForm()
	return render_template(
	"test.html",
	title="Quizz",form=f
	)


#/api/quizzs/id GET
#/api/quizzs/id/questions GET
#api/quizzs POST
#api/quizzs/id/questions POST
@app.route('/api/quizzs', methods=['GET'])
def get_quizzs():
	quizzs = Quizz.query.all()
	return jsonify({'quizzs': [ q.to_json() for q in quizzs ]})

@app.route('/api/quizzs/me', methods=['GET'])
def get_mes_quizzs():
	username=current_user.username
	quizzs = Quizz.query.filter(Quizz.username == username ).all()
	return jsonify({'quizzs': [ q.to_json() for q in quizzs ]})

@app.route('/api/quizzs', methods=['POST'])
def create_quizz():
	if not request.json or not 'name' in request.json:
		abort(400)
	username=current_user.username
	quizz = Quizz(name=request.json['name'],username=username)
	db.session.add(quizz)
	db.session.commit()
	return jsonify(quizz.to_json()), 201

@app.route('/api/quizzs/<int:id>', methods=['GET'])
def get_quizzId(id):
    quizz = Quizz.query.get_or_404(id)
    return jsonify(quizz.to_json())

@app.route('/api/questions/<int:id>', methods=['GET'])
def get_questionId(id):
    question = Question.query.get_or_404(id)
    return jsonify(question.to_json())

@app.route('/api/questions', methods=['GET'])
def get_questions():
    quests = SimpleQuestion.query.all()
    return jsonify({'questions': [ q.to_json() for q in quests ]})

@app.route('/api/quizzs/<int:id>/questions', methods=['GET'])
def get_quizzQuestions(id):
	quests = SimpleQuestion.query.filter(SimpleQuestion.quizz_id==id).all()
	return jsonify({'questions': [ q.to_json() for q in quests ]})

@app.route('/api/questions', methods=['POST'])
def create_question():
	quests = SimpleQuestion.query.all()
	if not request.json or not 'title' in request.json:
		abort(400)
	question = SimpleQuestion(
		title = request.json['title'],
		firstAlternative = request.json['firstAlternative'],
		secondAlternative = request.json['secondAlternative'],
        firstChecked = request.json['firstChecked'],
		quizz_id = request.json['quizz_id']
    )
	db.session.add(question)
	db.session.commit()
	return jsonify(question.to_json()), 201

@app.route('/api/users', methods=['GET'])
def get_users():
	users = User.query.all()
	return jsonify({'users': [ u.to_json() for u in users ]})


@app.route("/signin",methods=("GET","POST",))
def signin():
	f=SigninForm()
	if not f.is_submitted():
		f.next.data=request.args.get("next")
	elif f.validate_on_submit():
		try:
			m=sha256()
			m.update(f.password.data.encode())
			u=User(username=f.username.data,password=m.hexdigest())
			db.session.add(u)
			db.session.commit()
			next=f.next.data or url_for('home')
			login_user(u)
		except IntegrityError:
			return render_template("signin.html",form=f,LoginUtil=True)
		return redirect(next)
	return render_template("signin.html",form=f,LoginUtil=False)


#~ route pour la connexion
@app.route("/login",methods=("GET","POST",))
def login():
    f=LoginForm()
    if not f.is_submitted():
        f.next.data=request.args.get("next")
    elif f.validate_on_submit():
        user=f.get_authenticated_user()
        if user:
            login_user(user)
            next=url_for('home')
            return redirect(next)
    next=url_for('signin')
    return redirect(next)


#~route pour la deconnexion
@app.route("/logout/")
def logout():
	logout_user()
	return redirect(url_for('home'))
