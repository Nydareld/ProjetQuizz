from .app import db
from flask import url_for
from flask.ext.login import UserMixin
from .app import login_manager


@login_manager.user_loader
def load_user(username):
	return User.query.get(username)

class User(db.Model,UserMixin):
	username	=db.Column(db.String(50),primary_key=True)
	password	=db.Column(db.String(64))

	def get_id(self):
		return self.username


class Quizz(db.Model):
	id   = db.Column(db.Integer, primary_key=True)
	name = db.Column(db.String(100))
	username = db.Column(db.String(50), db.ForeignKey("user.username"))

	def __init__(self, name, username):
		self.name = name
		self.username = username

	def __repr__(self):
		return "<Quizz (%d) %s>" % (self.id, self.name)

	def to_json(self):
		json_quizz = {
            'url': url_for('get_quizzs',
				id=self.id, _external=True),
			'name': self.name,
			'creator':self.username,
			'questions':
			[ q.to_json()['url'] for q in self.questions ],
			'id':self.id
		}
		return json_quizz

class Game(db.Model):
    id   = db.Column(db.Integer, primary_key=True)
    username1 = db.Column(db.String(50), db.ForeignKey("user.username"))
    user1 = db.relationship("User",
            backref=db.backref("gamesCreator",
            lazy="dynamic",
            cascade="all, delete-orphan"))

    username2 = db.Column(db.String(50), db.ForeignKey("user2.username"))
    user2 = db.relationship("User",
    		backref=db.backref("gameChallenger",
			lazy="dynamic",
        	cascade="all, delete-orphan"))

    quizz_id = db.Column(db.Integer, db.ForeignKey('quizz.id'))
    quizz = db.relationship("Quizz",
        backref=db.backref("games",
        lazy="dynamic",
        cascade="all, delete-orphan"))

class Reponse(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    reponse = db.Column(db.String(120))
    success = db.Column(db.Boolean)
    question_id = db.Column(db.Integer, db.ForeignKey('question.id'))
    question = db.relationship("Question",
		backref=db.backref("reponses",
		lazy="dynamic",
		cascade="all, delete-orphan"))

class Question(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(120))
    quizz_id = db.Column(db.Integer, db.ForeignKey('quizz.id'))
    quizz = db.relationship("Quizz",
        backref=db.backref("questions",
        lazy="dynamic",
        cascade="all, delete-orphan"))

    def __repr__(self):
        return "<Question (%d) %s>" % (self.id, self.title)

    def to_json(self):
        json_quest = {
            'url': url_for('get_questionId',
             id=self.id, _external=True),
            'quizz_url': url_for('get_quizzId'
                , id=self.quizz_id,
                 _external=True),
            'title': self.title,
            'question-type': self.__class__.__name__
            }
        return json_quest

class SimpleQuestion(Question):
    firstAlternative = db.Column(db.String(120))
    secondAlternative = db.Column(db.String(120))
    firstChecked = db.Column(db.Boolean)

    def to_json(self):
        json_quest = super(SimpleQuestion, self).to_json()
        json_quest['firstAlternative'] = self.firstAlternative
        json_quest['secondAlternative'] = self.secondAlternative
        json_quest['firstChecked'] = self.firstChecked
        json_quest['quizz_id'] = self.quizz_id
        json_quest['id'] = self.id
        return json_quest
