from .app import manager, db

@manager.command
def syncdb(filename):
	'''
	Cree les tables et les remplit avec
	les données du fichier json
	'''

@manager.command
def syncdb():
	'''Creates all missing tables '''
	db.create_all()
